set title
set shiftwidth=2
set tabstop=2
set expandtab
set nu

set background=dark
set t_Co=256

set hlsearch

syntax on
"syntax enable

colorscheme molokai


let g:molokai_original = 1
let g:rehash256 = 1
set background=dark


set splitright

set tabpagemax=1000
let php_folding=1
setlocal cursorline
setlocal cursorcolumn
autocmd WinEnter * setlocal cursorline
autocmd WinEnter * setlocal cursorcolumn
autocmd WinLeave * setlocal nocursorline
autocmd WinLeave * setlocal nocursorcolumn
set makeprg=php\ -l\ %
set errorformat=%m\ in\ %f\ on\ line\ %l
filetype plugin on
filetype indent on

inoremap <silent> jj <ESC>


autocmd FileType vue syntax sync fromstart

" 分割関連 https://qiita.com/tekkoc/items/98adcadfa4bdc8b5a6ca
nnoremap ss :<C-u>sp<CR>
nnoremap sv :<C-u>vs<CR>
nnoremap sj <C-w>j
nnoremap sk <C-w>k
nnoremap sl <C-w>l
nnoremap sh <C-w>h
nnoremap sJ <C-w>J
nnoremap sK <C-w>K
nnoremap sL <C-w>L
nnoremap sH <C-w>H
nnoremap sq :<C-u>q<CR>


let g:ctrlp_map = '<c-p>'

" ctrlp の履歴検索(history)
nnoremap H :<C-u>CtrlPMRUFiles<CR>

" ag とうてば :CtrlSF となるように
nnoremap ag :<C-u>CtrlSF 



" Guess vcs root dir
let g:ctrlp_working_path_mode = 'ra'



" 大文字小文字変換無効
vmap u <Nop>
vmap U <Nop>
vmap ~ <Nop>

" vueをhtmlとして認識
autocmd BufNewFile,BufRead *.{html,htm,vue*} set filetype=html


" setlocal omnifunc=syntaxcomplete#Complete
" 
" " tabでオムニ補完
" function InsertTabWrapper()
"     if pumvisible()
"         return "\<c-n>"
"     endif
"     let col = col('.') - 1
"     if !col || getline('.')[col - 1] !~ '\k\|<\|/'
"         return "\<tab>"
"     elseif exists('&omnifunc') && &omnifunc == ''
"         return "\<c-n>"
"     else
"         return "\<c-x>\<c-o>"
"     endif
" endfunction
" inoremap <tab> <c-r>=InsertTabWrapper()<cr>
" 
" " rubycomplete.vim
" let g:rubycomplete_buffer_loading = 1
" let g:rubycomplete_rails = 1
" let g:rubycomplete_classes_in_global = 1
" 
set enc=utf-8
set fenc=utf-8
set fencs=utf-8,iso-2022-jp,euc-jp,cp932

set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P           
set laststatus=2

if exists('&ambiwidth')
  set ambiwidth=double
endif

source $VIMRUNTIME/macros/matchit.vim
"syntax on
filetype on
au FileType ruby set ts=2 sw=2 expandtab

set guioptions=r
set guioptions+=a
set clipboard=unnamed

highlight CursorLine cterm=none ctermbg=Black
highlight CursorColumn ctermfg=Green

nnoremap <C-K><C-K> cw<C-R>0<C-[>
nnoremap <C-K><C-I> ciw<C-R>0<C-[>

" yanktmpプラグイン
map <silent> sy :call YanktmpYank()<cr>
map <silent> sp :call YanktmpPaste_p()<cr>
map <silent> sP :call YanktmpPaste_P()<cr> 

" ベル無効
set visualbell t_vb=

" インデントで折りたたみ
" set foldmethod=indent
" set foldlevel=3





" Enable CursorLine
set cursorline

" Default Colors for CursorLine
highlight  CursorLine ctermbg=Black ctermfg=None

" Change Color when entering Insert Mode
" autocmd InsertEnter * highlight CursorLine ctermbg=Black ctermfg=Red
autocmd InsertEnter * highlight CursorLine cterm=underline ctermfg=NONE ctermbg=NONE

" Revert Color to default when leaving Insert Mode
" autocmd InsertLeave * highlight  CursorLine ctermbg=DarkGray ctermfg=None
autocmd InsertLeave * highlight CursorLine cterm=NONE ctermfg=NONE ctermbg=NONE


" Turn off paste mode when leaving insert
autocmd InsertLeave * set nopaste


" ペースト時にインデントしないように
if &term =~ "xterm"
  let &t_SI .= "\e[?2004h"
  let &t_EI .= "\e[?2004l"
  let &pastetoggle = "\e[201~"

  function XTermPasteBegin(ret)
      set paste
      return a:ret
  endfunction

  inoremap <special> <expr> <Esc>[200~ XTermPasteBegin("")
endif




" vim-plug ここから

"
call plug#begin('~/.vim/plugged')

Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails'

" GBlameとか
Plug 'tpope/vim-fugitive'

Plug 'slim-template/vim-slim'
Plug 'tpope/vim-haml'
Plug 'digitaltoad/vim-pug'

" ファイルオープンを便利に
Plug 'Shougo/unite.vim'

" Unite.vimで最近使ったファイルを表示できるようにする
" Plug 'Shougo/neomru.vim'

" 補完
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

" インデント可視化
" Plug 'Yggdroot/indentLine'

" ruby の end 補完
Plug 'tpope/vim-endwise'

" 検索
Plug 'rking/ag.vim'

" " ruby シンタックスチェック
" " Plug 'scrooloose/syntastic'
" Plug 'vim-syntastic/syntastic'

" ファイル絞り込み
Plug 'ctrlpvim/ctrlp.vim'

Plug 'yegappan/mru'


Plug 'dyng/ctrlsf.vim'

" Plug 'hail2u/vim-css3-syntax'
Plug 'JulesWang/css.vim'
Plug 'ap/vim-css-color'

" Coc is an intellisense engine for vim8 & neovim.
" CSSのハイライトはこれで
Plug 'neoclide/coc.nvim', {'branch': 'release'}




" call dein#add('Shougo/neco-syntax')
call plug#end()

" vim-plug ここまで

" スペース2回で履歴表示
nnoremap <space><space> :<c-u>MRU<CR>

" --- type ° to search the word in all files in the current dir
nmap ' :Ag <c-r>=expand("<cword>")<cr><cr>
nnoremap <space>/ :Ag


autocmd BufNewFile,BufRead *.slim set ft=slim
let g:deoplete#enable_at_startup = 1

" ctrlp 設定
" ag を使うようにする
let g:ctrlp_user_command='ag %s -i --nocolor --nogroup -g ""'
let g:ctrlp_match_window = 'max:30,order:ttb'
set guifont=Cica:h12
set printfont=Cica:h12
set ambiwidth=double
" エンターで開けるように
" let g:ctrlp_prompt_mappings = {
"     \ 'AcceptSelection("t")': ['<cr>'],
"     \ }


" vim-lspでrubyを補完するための設定
if executable('solargraph')
  " gem install solargraph
  au User lsp_setup call lsp#register_server({
      \ 'name': 'solargraph',
      \ 'cmd': {server_info->[&shell, &shellcmdflag, 'solargraph stdio']},
      \ 'initialization_options': {"diagnostics": "true"},
      \ 'whitelist': ['ruby'],
      \ })
end




let mapleader = "\<Space>"
nnoremap <Leader>o :CtrlP<CR>
nnoremap <Leader>w :w<CR>
" 「s」：ウィンドウを縦分割
nnoremap <Leader>s :<C-u>sp<CR>
" 「v」：ウィンドウを横分割
nnoremap <Leader>v :<C-u>vs<CR>
" 「S」：ウィンドウを縦分割(ファイルを選択)
nnoremap <Leader>S :<C-u>sp <TAB>
" 「V」：ウィンドウを横分割（ファイルを選択）
nnoremap <Leader>V :<C-u>vs <TAB>
" 「,m」：マウスモードOFF
noremap <Leader>m :<C-u>set mouse=<CR>:set nonumber<CR>
" 「,M」：マウスモードON
noremap <Leader>M :<C-u>set mouse=a<CR>:set number<CR>
" 「,r」：.vimrcのリロード
noremap <Leader>r :source ~/.vimrc<CR>:noh<CR>




" " --------------------------------
" " syntastic
" " --------------------------------
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
" 
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
" 
" let g:syntastic_mode_map = { 'mode': 'active', 'active_filetypes': [
"   \ 'ruby', 'javascript','coffee', 'scss', 'html', 'haml', 'slim', 'sh',
"     \ 'spec', 'vim', 'zsh', 'sass', 'eruby'] }
" 
"     let g:syntastic_javascript_checkers=['eslint']
"     let g:syntastic_coffee_checkers = ['coffeelint']
"     let g:syntastic_scss_checkers = ['scss_lint']
"     let g:syntastic_ruby_checkers = ['rubocop']
" 
"     let g:syntastic_error_symbol='✗'
"     let g:syntastic_style_error_symbol = '✗'
"     let g:syntastic_warning_symbol = '⚠'
"     let g:syntastic_style_warning_symbol = '⚠'


"   " vundleおまじない
"   set rtp+=~/.vim/vundle.git/
"   call vundle#rc()
"    
"   " vim-scripts リポジトリ (1)
"   Bundle 'Shougo/neocomplcache'
"   Bundle 'Shougo/neosnippet'
"   
"   " Bundle 'snipMate'
"   Bundle 'tpope/vim-rails'
"   
"   " Glameとか
"   Bundle 'tpope/vim-fugitive'
"   
"   " ツリー表示(:NERTTree コマンド)
"   Bundle 'scrooloose/nerdtree'
"   
"   " コメントON/OFFを手軽に実行(Ctrl + ハイフン２回)
"   Bundle 'tomtom/tcomment_vim'
"   
"   Bundle "ctrlpvim/ctrlp.vim"
"   
"   " ログファイルを色づけしてくれる
"   Bundle 'vim-scripts/AnsiEsc.vim'
"   
"   Plugin 'scrooloose/syntastic'
"   
"   Plugin 'kchmck/vim-coffee-script'
"   au BufRead,BufNewFile,BufReadPre *.coffee   set filetype=coffee
"   
"   " ジャンプ
"   Bundle 'szw/vim-tags'
"   " let g:vim_tags_project_tags_command = "/usr/bin/ctags -f .git/tags -R . 2>/dev/null"
"   " let g:vim_tags_gems_tags_command = "/usr/bin/ctags -R -f .git/Gemfile.lock.tags `bundle show --paths` 1>/dev/null"
"   
"   
"   " highlight slim
"   Bundle "slim-template/vim-slim"
"   autocmd BufNewFile,BufRead *.slim set ft=slim
"   
"   let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': ['ruby'] }
"   let g:syntastic_ruby_checkers = ['rubocop']
"   
"   " call vundle#begin()
"   " Plugin 'wincent/command-t'
"   " call vundle#end()
"   
"   Bundle "sophacles/vim-processing"
"   
"   Plugin 'tpope/vim-endwise'
