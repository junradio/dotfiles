# Lines configured by zsh-newuser-install                                                                                                                                                        
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000

setopt hist_ignore_all_dups  # 重複するコマンド行は古い方を削除
setopt hist_ignore_dups      # 直前と同じコマンドラインはヒストリに追加しない
setopt share_history         # コマンド履歴ファイルを共有する
setopt append_history        # 履歴を追加 (毎回 .zsh_history を作るのではなく)
setopt inc_append_history    # 履歴をインクリメンタルに追加
setopt hist_no_store         # historyコマンドは履歴に登録しない
setopt hist_reduce_blanks    # 余分な空白は詰めて記録
zstyle ':completion:*:default' menu select


bindkey -v
bindkey "^R" history-incremental-search-backward
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line


# The following lines were added by compinstall
zstyle :compinstall filename "~/.zshrc"
 
autoload -Uz compinit
compinit
# End of lines added by compinstall
 
autoload colors
colors



# vcs_infoを読み込み
autoload -Uz vcs_info
#  
# vcs_info_msg_0_変数をどのように表示するかフォーマットの指定
## デフォルトのフォーマット
### %r: リポジトリ名
### %b: ブランチ名
zstyle ':vcs_info:*' formats '[%r:%b]'
## 特別な状態（mergeでコンフリクトしたときなど）でのフォーマット
### %a: アクション名（merge, rebaseなど）
zstyle ':vcs_info:*' actionformats '[%r:%b|%a]'
 
# プロンプトが表示される毎にバージョン管理システムの情報を取得
## precmd: プロンプトが表示される毎に実行される関数
## vcs_info: バージョン管理システムから情報を取得
precmd () { vcs_info }

# 右プロンプトに表示
## prompt_subst: プロンプトを表示する際に変数を展開するオプション
setopt prompt_subst

## vcs_info_msg_0_: バージョン管理システムの情報
## RPROMPT: 右プロンプトに情報を表示するときの環境変数

PROMPT="%{${fg[green]}%}%n%{${reset_color}%}%{${fg[yellow]}%}@$(echo `hostname`) %(!.#.$) %{${reset_color}%}"
RPROMPT='%{${fg[green]}%}%~%{${reset_color}%} %{${fg[cyan]}%}${vcs_info_msg_0_}%{${reset_color}%}'
#RPROMPT=$RPROMPT'${vcs_info_msg_0_}'
SPROMPT="correct: %R -> %r ? " 

export PATH=/opt/local/bin:/opt/local/sbin/:/usr/sbin/:/usr/local/mysql/bin/:/user/local/bin/:$PATH

# export LS_COLORS='no=00:fi=00:di=04:ln=01;36:pi=40;33:so=40;33:bd=40;33:cd=40;33:ex=01;31:or=04;36:*.tgz=01;32:*.gz=01;32:*.tar=01;32:*.lzh=01;32:*.LZH=01;32:*.lha=01;32:*.zip=01;32:*.z=01;32:*.Z=01;32:*.rpm=01;32:*.gif=01;35:*.jpg=01;35:*.tif=01;35:*.eqs=01;35:*.ps=01;35:*.bmp=01;35:*.xwd=01;35:*.JPG=01;35:*.jpeg=01;35:*.obj=01;35'
export LSCOLORS=xbfxcxdxbxegedabagacad

export GIT_EDITOR=vim

# git 
export GIT_PAGER="lv -c -Ou8"

# rvm
[[ -s "~/.rvm/scripts/rvm" ]] && source "~/.rvm/scripts/rvm" 

# alias ls="ls --color"
alias ls="ls -G"
# alias l='exa -hla --git'
# alias ls='exa'

alias cdr="cd ~/railsroot/"
alias cds="cd ~/railsroot/spec"
alias cdc="cd ~/railsroot/app/controllers"
alias cdm="cd ~/railsroot/app/models"
alias cdv="cd ~/railsroot/app/views"

alias g='git branch'
alias gd='git diff'
alias gc='git checkout'
alias gcd='git checkout develop'
alias gs='git status'
alias gp='git pull'

alias v='vim'


PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

# iTermのタブの色を変える
tab-color() {
    echo -ne "\033]6;1;bg;red;brightness;$1\a"
    echo -ne "\033]6;1;bg;green;brightness;$2\a"
    echo -ne "\033]6;1;bg;blue;brightness;$3\a"
}
tab-reset() {
    echo -ne "\033]6;1;bg;*;default\a"
}
#tab-color 255 0 0
#tab-reset

# iTerm のタイトルを変える
#echo -ne "\e]1;app01.eastrer\a"

# rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# direnv
export EDITOR=vim
eval "$(direnv hook zsh)"

# mysql
export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"
export PATH=/usr/local/Cellar/postgresql/10.5/bin/:$PATH



alias tmux="TERM=screen-256color-bce tmux"



export FOREVER_BETA=true

export LDFLAGS="-L/usr/local/opt/v8@3.15/lib"
export CPPFLAGS="-I/usr/local/opt/v8@3.15/include"

export PATH="$HOME/.nodenv/bin:$PATH"
eval "$(nodenv init -)"


# export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"
# LDFLAGS="-L/usr/local/opt/openssl@1.1/lib"
# CPPFLAGS="-I/usr/local/opt/openssl@1.1/include"
# PKG_CONFIG_PATH="/usr/local/opt/openssl@1.1/lib/pkgconfig"


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/jun/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/jun/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/jun/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/jun/google-cloud-sdk/completion.zsh.inc'; fi

# reCAPTCHA annotate用
export GOOGLE_APPLICATION_CREDENTIALS="/Users/jun/gcp/sowxp-production-fcf4f4fc1ab2.json"
